//Global Vars
script_version = "1.0.8";
article_top_image_url = '';
duplicateHeaderImage = '';
headerPdfLink = '';
headerPdfThumbnailLink = '';
headerVideoLink = '';
fullScreenWidth = 0;

function removeStyleElements() {
    //console.log("removeStyleElements");
    var ignoreElements = ["TABLE", "TD", "TR"];
    var styleElements = document.body.getElementsByTagName('style');
    for (var i = 0; i < styleElements.length; i++) {
        //try {
        var e = styleElements[i];
        if(!contains(ignoreElements,e.tagName)){
            e.parentNode.removeChild(e);
        }
        //}catch (er){}

    }
}
function removeStyleAttributes() {
    //console.log("removeStyleAttributes");
    var ignoreElements = ["TABLE-", "TD-", "TR-", "SPAN-"];
    var elementsWithStyleAttr = document.body.querySelectorAll("[style]")
    for (var i = 0; i < elementsWithStyleAttr.length; i++) {
        if(!contains(ignoreElements,elementsWithStyleAttr[i].tagName)){
            var attr = elementsWithStyleAttr[i].getAttribute("style") +";";
            attr = attr.replace(/\s*(height|width|font-family|font-size)\s*:\s*.+?\s*(;|px|%)\s*/g,"");
            elementsWithStyleAttr[i].setAttribute("style", attr);
        }
    }

}

function checkImageSize(ele) {
    ele.removeAttribute("width");
    ele.removeAttribute("height");
    var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if(fullScreenWidth  > 0){
        screenWidth = fullScreenWidth;
    }
    if(ele.naturalWidth < (0.4 * screenWidth)){
        var leftMargin  = ele.style.cssFloat == "left" ? "0px" : "5px";
        var rightMargin  = ele.style.cssFloat == "right" ? "0px" : "5px";
        var sizeAttr = "; width:" + ele.naturalWidth + "px !important; margin: 4px "  + rightMargin +" 0px " + leftMargin + ";";
        ele.setAttribute('style',  ele.getAttribute('style') + sizeAttr);
        ele.classList.add("inline-image");
        console.log("Natural Width Adjusted : " + ele.naturalWidth)
    }
    else{
        ele.classList.add("full-width-image");
        console.log("Natural Width : " + ele.naturalWidth)
    }
}
function resizeImages() {
    //console.log("removeImageSizeAttributes");
    var imgElements = document.body.getElementsByTagName('img');
    //console.log("Images found :"); console.log(imgElements);
    for (var i = 0; i < imgElements.length; i++) {

        var ele = imgElements.item(i);
        if(ele.complete){
            checkImageSize(ele);
        }
        else{
            ele.addEventListener('load', function(){
                checkImageSize(ele);
            })
            ele.addEventListener('error', function() {
                alert('error')
            })
        }

    }
}

function removeImageAndLinksWith(url) {
    if(url != null){
        var element = document.body.querySelector("img[src='"+url+"']")
        if(element != undefined && element != null){
            if (element.parentElement.tagName == "A"){
                var a = element.parentElement;
                var link = a.getAttribute("href");
                console.log(link);
                if(link.indexOf(".pdf") > 0 ){
                    //element.setAttribute("src", "https://www.shareicon.net/data/128x128/2017/05/06/885703_folder_512x512.png");
                }
                else if(link.indexOf(".mp4") > 0 ){
                    return true;
                }
                else if(link.indexOf(".mov") > 0 ){
                    return true;
                }
                else if(element.parentElement.parentElement.tagName == "P"){
                    console.log("Its an Image");
                    var p = element.parentElement.parentElement
                    p.removeChild(element.parentElement);
                    if(p.innerHTML.trim() == ""){
                        p.parentElement.removeChild(p);
                    }
                    else{
                        p.removeChild(a);
                    }
                }
                else{
                    a.parentElement.removeChild(a);
                }

            }
            else{
                element.parentElement.removeChild(element)
            }
        }
    }
}

function removeFirstEmptyParagraph() {
    var elements = document.body.children;
    if (elements.item(0).tagName == "P" && elements.item(0).innerHTML.trim() == "") {
        var p = elements.item(0);
        p.parentElement.removeChild(p);
    }
}

function removeFirstHeaderImage() {
    try {
        var elements = document.body.children;
        var a_img = null;
        if (elements.item(0).tagName == "P") {
            var p = elements.item(0);
            if(p.innerHTML.trim() == ""){
                p.parentElement.removeChild(p);
            }
            if (p.children.item(0).tagName == "A" && p.children.item(0).children.item(0).tagName == "IMG") {
                a_img = p.children.item(0)
            }
            else {//console.log(elements)
            }
        }
        else if(elements.item(0).tagName == "A" && elements.item(0).children.item(0).tagName == "IMG"){
            a_img = p.children.item(0)
        }
        else {console.log(elements)}

        if(a_img != null){
            if(a_img.parentNode.tagName == "P"){
                a_img.parentNode.parentNode.removeChild(a_img.parentNode)
            }
            else{
                a_img.parentNode.removeChild(a_img)
            }
        }

    }
    catch (er){
        console.log(er);
    }
}

function classFirstHeaderImage(className) {
    try {
        var firstElement0 = document.body.children.item(0);
        var firstElement1 = firstElement0.children.item(0) || firstElement0;
        if(firstElement1.tagName != "IMG" && firstElement1.innerHTML === ""){
            firstElement1 = firstElement0.children.item(1) || firstElement0;
        }
        var firstElement2 = firstElement1.children.item(0) || firstElement1;
        if(firstElement2.tagName != "IMG" && firstElement2.innerHTML === ""){
            firstElement2 = firstElement1.children.item(1) || firstElement1;
        }
        var firstElement3 = firstElement2.children.item(0) || firstElement2;
        if(firstElement3.tagName != "IMG" && firstElement3.innerHTML === ""){
            firstElement3 = firstElement2.children.item(1) || firstElement2;
        }
        var firstElement4 = firstElement3.children.item(0) || firstElement3;
        if(firstElement4.tagName != "IMG" && firstElement4.innerHTML === ""){
            firstElement4 = firstElement3.children.item(1) || firstElement3;
        }
        var firstElement5 = firstElement4.children.item(0) || firstElement4;
        if(firstElement5.tagName != "IMG" && firstElement4.innerHTML === ""){
            firstElement5 = firstElement4.children.item(1) || firstElement4;
        }

        var headerImageElement = null;
        if(firstElement0.tagName == "IMG" && firstElement1.innerHTML === ""){
            headerImageElement = firstElement0;
        }
        else if (firstElement1 != undefined && firstElement1.tagName == "IMG") {
            headerImageElement = firstElement1;
        }
        else if (firstElement2 != undefined && firstElement2.tagName == "IMG") {
            headerImageElement = firstElement2;
        }
        else if (firstElement3 != undefined && firstElement3.tagName == "IMG") {
            headerImageElement = firstElement3;
        }
        else if (firstElement4 != undefined && firstElement4.tagName == "IMG" && firstElement2.tagName == "SPAN") {
            headerImageElement = firstElement4;
        }
        else if (firstElement5 != undefined && firstElement5.tagName == "IMG" && firstElement3.tagName == "SPAN") {
            headerImageElement = firstElement5;
        }


        if(headerImageElement != null){

            var imageLink = headerImageElement.getAttribute("src");
            console.log(headerImageElement.getAttribute("src"))
            if(imageLink ===  ""){
                return false;
            }
            headerImageElement.classList.add(className);
            var imageWrapperElement = headerImageElement.parentElement;
            console.log(imageWrapperElement.tagName)
            if( imageWrapperElement.tagName === "A"){
                var wrapperLink = imageWrapperElement.getAttribute("href");
                if(wrapperLink.indexOf(".jpg") > 0 || wrapperLink.indexOf(".jpeg") > 0 || wrapperLink.indexOf(".png") > 0){
                    headerImageElement.parentElement.classList.add(className);
                    duplicateHeaderImage = wrapperLink;
                }
            }
            else{
                duplicateHeaderImage = headerImageElement.getAttribute("src");
                headerImageElement.parentElement.remove(headerImageElement);
            }
        }

    }
    catch (er){
        console.log(er);
    }
}

function findHeaderVideo() {
    try {
        var elements = document.body.children;
        var a_img = null;
        if (elements.item(0).tagName == "P") {
            var p = elements.item(0);
            if(p.innerHTML.trim() === ""){
                p.parentElement.removeChild(p);
                findHeaderVideo();
                return
            }
            if (p.children.item(0).tagName == "A" && p.children.item(0).children.item(0).tagName == "IMG"  && p.children.item(1) != undefined && p.children.item(1).tagName == "A") {
                var aEle = p.children.item(0);
                var imgEle = aEle.children.item(0);
                var aLinkEle = p.children.item(1);


                if(aEle.getAttribute("href").lastIndexOf(".pdf") < 0  &&
                    aEle.getAttribute("onclick").length > 0 &&
                    (aLinkEle.getAttribute("href").indexOf(".mp4") > 0 ||  aLinkEle.getAttribute("href").indexOf(".mov")))
                {
                    headerVideoLink = aLinkEle.getAttribute("href");
                    a_img = p.children.item(0);
                }


            }
            else {//console.log(elements)
            }
        }
        else {console.log(elements)}

        if(a_img != null){
            a_img.classList.add("header-video-wrapper");
            return true;
        }

        return false;

    }
    catch (er){
        console.log(er);
        return false;
    }
}

function findHeaderPdf() {
    try {

        var ele = document.querySelector("body p:first-of-type a#pdfpublicurl");
        if (ele != undefined && ele != null){
            if(ele.getAttribute("href").lastIndexOf(".pdf") > 0){
                headerPdfLink =  ele.getAttribute("href");
                duplicateHeaderImage = null;
                console.log(headerPdfLink)
                if (ele.childElementCount > 0 && ele.children.item(0).tagName == "IMG"){
                    headerPdfThumbnailLink = ele.children.item(0).getAttribute("src");
                    //duplicateHeaderImage = headerPdfThumbnailLink;
                }
                ele.parentElement.removeChild(ele);
                return true;
            }

        }

        return false;

    }
    catch (er){
        console.log(er);
        return false;
    }
}

function findHeaderlineImage() {
    try {

        var ele = document.querySelector("#headlineImage a img");
        if (ele != undefined && ele != null){
            var link = ele.parentElement.getAttribute("href") || ele.getAttribute("src");
            if(link.indexOf(".jpg") > 0 || link.indexOf(".jpeg") > 0 || link.indexOf(".png") > 0){
                duplicateHeaderImage = link;
                return true
            }
        }

        return false;

    }
    catch (er){
        console.log(er);
        return false;
    }
}


function removeAllElementsWithAttributeSetToValue(ele, attr, val) {
    var elements = document.body.getElementsByTagName(ele);
    console.log(elements)
    for(var i =0; i < elements.length; i++){
        console.log(i); console.log(elements[i]);
        var e = elements[i];
        var aVal = e.getAttribute(attr)
        if(aVal === undefined || aVal == null)
            return;
        if(aVal === val) {
            //console.log(e)
            console.log("removing element with "+attr+" = " + e.getAttribute(attr));
            e.parentNode.removeChild(e);

        }
        else{
            //console.log("Attribute Not Match");
            //console.log(e.getAttribute(attr));
            //console.log(val);
        }
    }
}

function convertSectionHeadingsTo_H_Elements(){
    var parahraphElements = document.body.querySelectorAll("p");
    for(var i = 0; i < parahraphElements.length; i++){
        var isHeading = false;
        var paragraphElement = parahraphElements.item(i);

        if(paragraphElement.childElementCount > 0){
            isHeading = true;//Assume true till proven otherwise
        }


        var innerContent = "";
        for(var j = 0; j < paragraphElement.childElementCount; j++) {
            if(paragraphElement.children.item(j).tagName != "STRONG"){
                isHeading = false;
                break;
            }
            else{
                innerContent += paragraphElement.children.item(j).outerHTML;
            }
        }

        if(paragraphElement.innerHTML != innerContent){
            isHeading = false;
            //console.log(paragraphElement.innerHTML)
            //console.log(innerContent)
        }

        if(isHeading){
            var h = document.createElement('h4');
            h.innerHTML = paragraphElement.innerHTML;
            paragraphElement.parentNode.replaceChild(h, paragraphElement);
        }
    }
}

function convertQuoteOnlySectionsTo_Blockquote_Elements() {
    var parahraphElements = document.body.querySelectorAll("p");
    for(var i = 0; i < parahraphElements.length; i++){
        var isQuote = false;
        var paragraphElement = parahraphElements.item(i);

        if(paragraphElement.childElementCount > 0){
            if(paragraphElement.children.item(0).innerHTML.trim().substr(0,1) == '"') {
                isQuote = true;//Assume true till proven otherwise
            }
        }

        for(var j = 0; j < paragraphElement.childElementCount; j++) {
            if(paragraphElement.children.item(j).tagName != "EM"){
                isQuote = false;
                break;
            }
        }


        if(isQuote){
            var e = document.createElement('blockquote');
            e.innerHTML = paragraphElement.innerHTML;

            paragraphElement.parentNode.replaceChild(e, paragraphElement);
        }
    }
}

function findImageSectionsAndAssignClass(clsName){
    var imgElements = document.body.querySelectorAll('p>a>img');
    for(var i = 0; i < imgElements.length; i++) {
        imgElements[i].parentElement.parentElement.className += " " + clsName;
    }

    imgElements = document.body.querySelectorAll('p>img');
    for(var i = 0; i < imgElements.length; i++) {
        imgElements[i].parentElement.className += " " + clsName;
    }
}

function findInlineImagesAndAssignClass(clsName) {
    //span img,
    var inlineImages = document.body.querySelectorAll('img[style*="display:inline"], img[src^="data"]');
    for(var i = 0; i < inlineImages.length; i++) {
        inlineImages[i].className += " " + clsName;
    }

}

function resizeIFrames() {
    var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if(fullScreenWidth  > 0){
        screenWidth = fullScreenWidth;
    }
    var iframeElements = document.body.getElementsByTagName('iframe');
    for(var i = 0; i < iframeElements.length; i++) {
        var e = iframeElements.item(i);
        var old_width = e.getAttribute("width") || 0;
        var old_height = e.getAttribute("height") || 0;
        var new_width = screenWidth;
        var new_height = old_height > 0 ? (new_width * old_height / old_width ) : (screenWidth / 1.3333)
        e.setAttribute("width", new_width.toString())
        e.setAttribute("height", new_height.toString());

    }
}
function removeAttributeOnElements(attr, eleSelctor) {
    console.log("removeAttributeOnElements");
    var elements = document.body.querySelectorAll(eleSelctor)
    for (var i = 0; i < elements.length; i++) {
        try {
            elements.item(i).removeAttribute(attr);
        }catch (er){}
    }

}

function resizeTables() {

    var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if(fullScreenWidth  > 0){
        screenWidth = fullScreenWidth;
    }
    var tableElements = document.body.getElementsByTagName('table');
    for(var i = 0; i < tableElements.length; i++) {
        var e = tableElements.item(i);
        var old_width = e.getAttribute("width") || 0 ;
        var new_width = screenWidth;
        if(old_width > (new_width * 0.6)  || old_width == 0){
            //If width is set and is almost full screen
            e.setAttribute("width", new_width.toString());
            e.style.maxWidth = screenWidth + "px";
            e.classList.add("full-width-table")
        }
        else{
        }
    }
}

function preventLocalSrcAttributes() {
    var elements = document.body.querySelectorAll("[src]")
    for(var i = 0; i < elements.length; i++) {
        var attrValue = elements.item(i).getAttribute("src");
        if(attrValue.lastIndexOf("//", 0) === 0){
            elements.item(i).setAttribute("src", "http:" + attrValue)
        }
    }
}
function preventLocalHrefAttributes() {
    var elements = document.body.querySelectorAll("[href]")
    for(var i = 0; i < elements.length; i++) {
        var attrValue = elements.item(i).getAttribute("href");
        if(attrValue.lastIndexOf("//", 0) === 0){
            elements.item(i).setAttribute("href", "http:" + attrValue)
        }
    }
}

function maskElements(eleSelector) {
    var elementsToMask = document.body.querySelectorAll(eleSelector);
    var maskedElements = [];
    for(var i = 0; i < elementsToMask.length; i++) {
        var placeholderEle = document.createElement("masked");
        placeholderEle.id = "masked_" + i.toString();
        maskedElements[i] = elementsToMask.item(i).parentNode.replaceChild(placeholderEle,elementsToMask.item(i))
    }
    return maskedElements;
}

function unmaskElements(maskedElements) {
    for(var i = 0; i < maskedElements.length; i++) {
        var eleToUnMask = document.getElementById("masked_" + i.toString());
        eleToUnMask.parentNode.replaceChild(maskedElements[i],eleToUnMask)
    }
}

function findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}

function contains(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}

function findHeaderElement() {
    var foundHeaderElement = false;
    foundHeaderElement = findHeaderlineImage();
    if(!foundHeaderElement){
        try {
            foundHeaderElement = findHeaderVideo();
        }
        catch (e) {
        }
    }
    if(!foundHeaderElement){
        try{
            foundHeaderElement = findHeaderPdf();
        }
        catch (e){}
    }

    if(!foundHeaderElement){
        classFirstHeaderImage("top-image");
    }
}

reloadImages = function() {
    var imageEles = document.querySelectorAll("img");
    for(var i = 0; i < imageEles.length; i++) {
        var ele = imageEles.item(i);
        var x = ele.cloneNode(true);
        x.addEventListener('load', function(){
            checkImageSize(x);
        });
        ele.parentElement.replaceChild(x, ele);
    }

    resizeImages();
    return "Reloaded Images";
}

removeDummyImage = function(){
    var dummyImage = document.querySelector("p.dummy-image");
    if(dummyImage != undefined && dummyImage != null){
        dummyImage.parentElement.removeChild(dummyImage);
    }
}

function cleanHtmlContent() {
    console.log("cleanHtmlContent");


    //var maskedElements = maskElements("[id^=poll_id]");
    var maskedElements = maskElements(".quiz-object, .poll-container-text, .twitter-tweet, .twitter-tweet + script");

    removeStyleElements();
    removeFirstEmptyParagraph();
    //removeFirstHeaderImage()

    findHeaderElement();

    findInlineImagesAndAssignClass("inline-image");
    removeImageAndLinksWith(article_top_image_url);
    removeStyleAttributes();

    removeAttributeOnElements("allowfullscreen", "iframe");
    convertSectionHeadingsTo_H_Elements();
    convertQuoteOnlySectionsTo_Blockquote_Elements();
    findImageSectionsAndAssignClass("photo");
    resizeIFrames();
    resizeTables();
    preventLocalSrcAttributes();
    preventLocalHrefAttributes();


    reloadImages();
    //window.setTimeout(reloadImages, 100);

    unmaskElements(maskedElements);

}


function autoPlayVideo() {

}
